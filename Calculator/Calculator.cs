﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    class Calculator
    {
        public static string Calculate(double number_one, double number_two, string operation)
        {
            string result = String.Empty;

            switch (operation)
            {
                case "+":
                    result = Addition(number_one, number_two);
                    break;
                case "-":
                    result = Subtraction(number_one, number_two);
                    break;
                case "*":
                    result = Multiplication(number_one, number_two);
                    break;
                case "/":
                    if (number_two != 0)
                        result = Division(number_one, number_two);
                    else
                        result="Error: you cannot divide by zero";
                    break;

                default:
                    break;
            }
            return result;
        }

        static int GetDecimalDigitsCount(double number)
        {
            string[] str = number.ToString(new System.Globalization.NumberFormatInfo() { NumberDecimalSeparator = "." }).Split('.');
            return str.Length == 2 ? str[1].Length : 0;
        }



        private static string Addition(double num1, double num2)
        {
            string result = "";
            int count1 = GetDecimalDigitsCount(num1);
            int count2 = GetDecimalDigitsCount(num2);
            int count = 0;
            int lenth = 1;
            if (count1 > count2)
                count = count1;
            else
                count = count2;
            double res = Math.Round(num1 + num2, count);
            string num_1 = String.Format("{0:f" + count + "}", num1);
            string num_2 = String.Format("{0:f" + count + "}", num2);
            string _result = res.ToString();
            lenth += _result.Length;
            result = $"{num1} + {num2} = {res}\r\n";
            result += new String(' ', lenth - num_1.ToString().Length) + num_1.ToString() +
                "\r\n" + "+" + "\r\n" + new String(' ', lenth - num_2.ToString().Length) +
                num_2.ToString() + "\r\n" + new String('_', lenth) + "\r\n" + " " + _result + "\r\n";

            return result;
        }

        private static string Subtraction(double num1, double num2)
        {
            string result = "";
            int count1 = GetDecimalDigitsCount(num1);
            int count2 = GetDecimalDigitsCount(num2);
            int count = 0;
            int lenth = 1;
            if (count1 > count2)
                count = count1;
            else
                count = count2;
            string num_1 = String.Format("{0:f" + count + "}", num1);
            string num_2 = String.Format("{0:f" + count + "}", num2);
            string _result = "";
            double res = 0;
            if (num_1.Length > num_2.Length)
            {
                lenth += num_1.Length;
                res = Math.Round(num1 - num2, count);
            }
            else
            {
                lenth += num_2.Length;
                res = Math.Round(num2 - num1, count);
            }

            if (num1 >= num2)
            {
                _result = res.ToString();
                result = $"{num1} - {num2} = {res}\r\n";
                result += new String(' ', lenth - num_1.ToString().Length) + num_1.ToString() +
                "\r\n" + "-" + "\r\n" + new String(' ', lenth - num_2.ToString().Length) +
                num_2.ToString() + "\r\n" + new String('_', lenth) + "\r\n" + new String(' ', lenth - _result.Length) + _result;

            }
            else
            {
                _result = (res).ToString();
                result = $"{num1} - {num2} = -({num2} - {num1}) = {-(res)}\r\n";
                result += new String(' ', lenth - num_2.ToString().Length) + num_2.ToString() +
                "\r\n" + "-" + "\r\n" + new String(' ', lenth - num_1.ToString().Length) +
                num_1.ToString() + "\r\n" + new String('_', lenth) + "\r\n" + new String(' ', lenth - _result.Length) + _result;

            }

            return result;
        }


        private static string Multiplication(double num1, double num2)
        {
            string result = "";
            int count1 = GetDecimalDigitsCount(num1);
            int count2 = GetDecimalDigitsCount(num2);
            int count = count1 + count2;
            double res = Math.Round(num1 * num2, count);
            string _result = res.ToString();
            int lenth = 1;

            string num_1 = num1.ToString();
            string num_2 = num2.ToString();

            if (res != 0)
                lenth += _result.Length;
            else
            {
                if (num_1.Length > num_2.Length)
                    lenth += num_1.Length;
                else
                    lenth += num_2.Length;
            }

            double num = Math.Pow(10,count1);
            num = num1 * num;
            num = Math.Round(num, 0);

            char[] ar = num_2.ToCharArray();
            Array.Reverse(ar);
            num_2 = new String(ar);
            string add = "";
            result = $"{num1} * {num2} = {res}" + "\r\n";
            result += new String(' ', lenth - num1.ToString().Length) + num1.ToString() +
                "\r\n" + "*" + "\r\n" + new String(' ', lenth - num2.ToString().Length) +
                num2.ToString() + "\r\n" + new String('_', lenth) + "\r\n";
            if (num1 != 0 && num2 != 0)
            {
                foreach (char c in num_2)
                {
                    string str;
                    if (c != '.' && c != ',' && c != '0')
                    {
                        str = char.GetNumericValue(c) * num + add;
                        str = new String(' ', lenth - str.ToString().Length) + str.ToString();
                        result += str + "\r\n";
                        add += " ";
                    }
                    if (c == '0')
                        add += " ";

                }
                result += new String('_', lenth) + "\r\n";
                result += " " + _result + "\r\n";
            }
            else
                result += new String(' ', lenth - 1) + _result + "\r\n";
            return result;
        }

        private static string Division(double num1, double num2)
        {
            string result = "";
            double res = num1/num2;
            string _result = res.ToString();
            result = $"{num1} / {num2} = {res}" + "\r\n";
            return result;
        }
    }

}
