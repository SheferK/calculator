﻿using System;

namespace Calculator
{
    class Program
    {
        private static bool key = true;
        private static string _char;
        private static string input_numderone;
        private static string input_numdertwo;
        private static string operation;
        private static double num_1;
        private static double num_2;
        private static string result;

        static void Main(string[] args)
        {
            Console.WriteLine("Console Calculator\r");
            Console.WriteLine("------------------\n");

            while (key)
            {
                input_numderone = String.Empty;
                input_numdertwo = String.Empty;
                operation = String.Empty;
                result = "";
                _char = String.Empty;

                Console.Write("Type a number, and then press Enter: ");
                input_numderone = Console.ReadLine();

                while (!double.TryParse(input_numderone, out num_1))
                {
                    Console.Write("This is not valid input. Please enter an integer value: ");
                    input_numderone = Console.ReadLine();
                }

                // Ask the user to choose an operator.
                while (operation != "+" && operation != "-" && operation != "*" && operation != "/")
                {
                    Console.WriteLine("Choose an operator from the following list:");
                Console.WriteLine("\t+ \t- \t* \t/");
                Console.Write("Your option? ");
                operation = Console.ReadLine();
                }

                // Ask the user to type the second number.
                Console.Write("Type another number, and then press Enter: ");
                input_numdertwo = Console.ReadLine();

                while (!double.TryParse(input_numdertwo, out num_2))
                {
                    Console.Write("This is not valid input. Please enter an integer value: ");
                    input_numdertwo = Console.ReadLine();
                }

                try
                {
                    result = Calculator.Calculate(num_1, num_2, operation);
                    Console.WriteLine($"Your result: \n {result}");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }

                Console.WriteLine("------------------------\n");

                // Wait for the user to respond before closing.
                while (_char == String.Empty)
                {
                    Console.Write("Press 'n' and Enter to close the app, or press 'y' and Enter to continue: ");
                    _char = Console.ReadLine();
                    if (_char == "n")
                        key = false;
                    else if (_char == "y")
                        key = true;
                    else
                        _char = String.Empty;
                }
                Console.WriteLine("\n");
            }
            return;
        }
    }
}
